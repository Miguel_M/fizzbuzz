let esFizz = function(numero){
  if(numero % 3 === 0)
  {
    return true;
  }
  else
  {
    return false;
  }
}
let esBuzz = function(numero){
  if(numero % 5 === 0)
  {
    return true;
  }
  else
  {
    return false;
  }
}
let esFizzBuzz = function(numero){
  if(esFizz(numero)&&esBuzz(numero))
  {
    return true;
  }
  else
  {
    return false;
  }
}
function fizzbuzz (tamano) {
  let numero
  for(numero = 0; numero <= tamano; numero++) 
  {
      if (esFizzBuzz(numero))
      {
        console.log("FizzBuzz");                    
      }
      else 
        if(esFizz(numero))
        {
          console.log("Fizz");
        }
        else 
          if(esBuzz(numero))
          {
            console.log("Buzz");
          }
          else
            console.log(numero);
  }
}

fizzbuzz(100)

module.exports={
  esFizz: esFizz,
  esBuzz: esBuzz,
  esFizzBuzz: esFizzBuzz
}