var assert = require('assert');
var expect = require('chai').expect;
var should = require('chai').should();
var fizzbuzz = require('../fizzbuzz');

describe('esFizz', function(){
    
    it("Retornar 'true' cuando ingrese 3",function(){
        expect(fizzbuzz.esFizz(3)).to.equal(true);        
    });
    it("Retornar 'false' cuando ingrese 4",function(){
        expect(fizzbuzz.esFizz(4)).to.equal(false);        
    });
});
describe('esBuzz', function(){
    
    it("Retornar 'true' cuando ingrese 5",function(){
        expect(fizzbuzz.esBuzz(5)).to.equal(true);        
    });
    it("Retornar 'false' cuando ingrese 4",function(){
        expect(fizzbuzz.esBuzz(4)).to.equal(false);        
    });
});

describe('esFizzBuzz', function(){
    
    it("Retornar 'true' cuando ingrese 15",function(){
        expect(fizzbuzz.esFizzBuzz(15)).to.equal(true);        
    });
    it("Retornar 'false' cuando ingrese 4",function(){
        expect(fizzbuzz.esFizzBuzz(4)).to.equal(false);        
    });
});